// compose :: ((b -> c), (a -> b), ...) -> a -> c
var compose = function() {
		var fns = [].slice.apply(arguments)
		return function() {
			return fns.reduceRight(function(arg, fn) {
				return fn(arg)
			}, [].slice.apply(arguments)[0])
		}
	}

// concat :: [a] -> [a] -> [a]
var concat = function(x, y) { return x.concat(y) }

// curry :: (* -> a) -> (* -> a)
var curry = function(fn, argList) {
		var args = argList || []
		return function() {
			var totalArgs = args.concat([].slice.apply(arguments))
			return totalArgs.length < fn.length ?
				curry(fn, totalArgs) :
				fn.apply(null, totalArgs)
		}
	}

// each :: (* -> a) -> [a] -> [a]
var each = curry(function(fn, obj) {
		if(obj.forEach instanceof Function) {
			obj.forEach(fn)
		} else {
			for(var key in obj) {
				if(obj.hasOwnProperty(key)) {
					fn(obj[key], key, obj)
				}
			}
		}
		return obj
	})

// filter :: (a -> Bool) -> [a] -> [a]
var filter = curry(function(fn, obj) {
		if(obj.filter instanceof Function) {
			return obj.filter(fn)
		} else {
			var output = {};
			for(var key in obj) {
				if(obj.hasOwnProperty(key)) {
					if(fn(obj[key], key)) output[key] = obj[key]
				}
			}
			return output
		}
	})

// fst :: [a] -> a
var fst = head

// head :: [a] -> a
var head = function(xs) { return nth(0, contractArr(xs)) }

// identity :: a -> a
var identity = function(x) { return x }

// last :: [a] -> a
var last = function(xs) {
		return contractArr(xs).length > 0 ? xs[xs.length - 1] : xs[0]
	}

// log :: a -> a
var log = function(x) {
		console.log(x)
		return x
	}

// map :: Functor f => (a -> b) -> f a -> f b
// map :: (a -> b) -> {a} -> {b}
var map = curry(function(fn, obj) {
		if(obj.map instanceof Function) {
			return obj.map(fn)
		} else {
			var output = {}
			for(var key in obj) {
				if(obj.hasOwnProperty(key)) {
					output[key] = fn(obj[key], key, obj)
				}
			}
			return output
		}
	})

// mconcat :: Monoid m => [m] -> m
var mconcat = function() { return [].slice.apply(arguments).reduce(concat) }

// memoize :: (* -> a) -> (* -> a)
var memoize = function(fn) {
		var store = {}
		return function() {
			var args = [].slice.apply(arguments)
			var key = fn.toString().concat(JSON.stringify(args))
			if(!store.hasOwnProperty(key)) {
				store[key] = fn.apply(null, args)
			}
			return store[key]
		}
	}

// nth :: Number -> [a] -> a | Undefined
// nth :: Number -> String -> String | Undefined
var nth = function(n, xs) {
		var idx = n < 0 ? n + xs.length : n
		return (typeof xs === 'string') ? xs.charAt(idx) : xs[idx]
	}

// of :: a -> [a]
var of = function() { return [].slice.apply(arguments) }

// prop :: s -> {s: a} -> a
var prop = curry(function(key, obj) { return obj[key] })

// path :: [String] -> {k: v} -> v
var path = curry(function(path, obj) {
		return path.reduce(function(acc, elem) {
			return acc !== undefined ||
				acc !== null ?
				acc[elem] : acc
		}, obj)
	})

// reduce :: (b -> a -> b) -> b -> [a] -> b
// reduce :: (b -> a -> b) -> b -> {a} -> b
var reduce = curry(function(fn, acc, obj) {
		if(obj.reduce instanceof Function) {
			return obj.reduce(fn)
		} else {
			var output = acc;
			for(var key in obj) {
				if(obj.hasOwnProperty(key)) {
					output = fn(output, obj[key], key)
				}
			}
			return output
		}
	})

// snd :: [a] -> a
var snd = function(xs) { return nth(1, contractArr(xs)) }

// tail :: [a] -> a
var tail = function(xs) { contractArr(xs).slice(1) }

// take :: Number -> [a] -> a
var take = curry(function(n, obj) {
		var xs
		if(obj instanceof Function) {
			xs = obj
		} else {
			xs = values(xs)
		}
		return xs.slice(0, n)
	})

// types :: String -> a -> a | throw
var types = curry(function(type, x) {
		if('[object ' + type + ']' === {}.toString.call(x)) return x
		throw 'Expect ' + x + ' to be type of ' + type
	})

var contractNum = types('Number')
var contractStr = types('String')
var contractArr = types('Array')
var contractObj = types('Object')
var contractFun = types('Function')
var contractUnd = types('Undefined')
var contractNul = types('Null')
var contractBool = types('Boolean')

// getInstance :: Object a, b => a -> b -> a | b
var getInstance = function(self, constructor) {
		return self instanceof constructor ?
			self :
			Object.create(constructor.prototype);
	}

///////////////////////////////////////////////////////////////////////////////
// MONOID CONSTRUCTORS ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// monoid :: Monoid m => (String, (_ -> m), (m -> m), (a -> a | throw)) -> m
var monoid = function(name, emptyFn, concatFn, contractFn) {
		if(!name) throw 'Expected name'
		if(!emptyFn) throw 'Expected empty function'
		if(!concatFn) throw 'Expected concat function'
		if(!contractFn) throw 'Expected contract function'

		function _(x) {
			var self = getInstance(this, _)
			self.x = contractFn(x) 
			self.empty = emptyFn
			self.concat = concatFn
			self.toString = function() {
				return name + '(' + this.x + ')'
			}

			return self;
		}
		return _;
	}

// Sum :: Monoid Number
var Sum = monoid(
		'Sum',
		function() { return Sum(0) },
		function(m) { return Sum(this.x + m.x) },
		contractNum
	)

// Prod :: Monoid Number
var Prod = monoid(
		'Prod',
		function() { return Prod(0) },
		function(m) { return Prod(this.x * m.x) },
		contractNum
	)

// Diff :: Monoid Number
var Diff = monoid(
		'Diff',
		function() { return Diff(0) },
		function(m) { return Diff(this.x - m.x) },
		contractNum
	)

// Div :: Monoid Number
var Div = monoid(
		'Div',
		function() { return Div(0) },
		function(m) { return Div(this.x / m.x) },
		contractNum
	)

// Min :: Monoid Number
var Min = monoid(
		'Min',
		function() { return Min(Number.MIN_VALUE) },
		function(m) { return Min(this.x < m.x ? this.x : m.x) },
		contractNum
	)

// Max :: Monoid Number
var Max = monoid(
		'Max',
		function() { return Max(Number.MAX_VALUE) },
		function(m) { return Max(this.x > m.x ? this.x : m.x) },
		contractNum
	)

// Any :: Monoid Bool
var Any = monoid(
		'Any',
		function() { return Any(false) },
		function(m) { return Any(this.x || m.x) },
		contractBool
	)

// Any :: Monoid Bool
var All = monoid(
		'All',
		function() { return All(true) },
		function(m) { return All(this.x && m.x) },
		contractBool
	)

// Path :: Monoid String
var Path = monoid(
		'Path',
		function() { return Path('/') },
		function(m) { return Path(this.x + '/' + m.x) },
		contractStr
	)

// TupleM :: Monoid [Monoid]
var TupleM = monoid(
		'TupleM',
		function() { return TupleM([Any.empty(), Any.empty()]) },
		function(m) {
			return TupleM([
				mconcat(this.x[0], m.x[0]),
				mconcat(this.x[1], m.x[1])
			])
		},
		function(x) {
			contractArr(x)
			if(typeof x[0].empty === 'function' && typeof x[1].empty) return x
			throw 'Expected ' + x + ' to be an array of monoids'
		}
	)

// Intersect :: Monoid []
var Intersect = monoid(
		'Intersect',
		function() { return Intersect([]) },
		function(m) {
			var value = this.x.reduce(function(acc, x) {
				if(~m.x.indexOf(x)) acc.push(x)
				return acc
			}, [])
			return Intersect(value)
		},
		contractArr
	)

// Unique :: Monoid []
var Unique = monoid(
		'Unique',
		function() { return Unique([]) },
		function(m) {
			var self = this
			var value = m.x.reduce(function(acc, x) {
				if(!(~self.x.indexOf(x))) acc.push(x)
				return acc
			}, self.x.slice())
			return Unique(value)
		},
		contractArr
	)

// MergeL :: Monoid {}
var MergeL = monoid(
		'MergeL',
		function() { return MergeL({}) },
		function(m) {
			var self = this
			var keys = Object.keys(this.x).concat(Object.keys(m.x))
			var value = keys.reduce(function(acc, key) {
				acc[key] = self.x[key] || m.x[key]
				return acc
			}, {})
			return MergeL(value)
		},
		contractObj
	)

// MergeR :: Monoid {}
var MergeR = monoid(
		'MergeR',
		function() { return MergeR({}) },
		function(m) {
			var self = this
			var keys = Object.keys(this.x).concat(Object.keys(m.x))
			var value = keys.reduce(function(acc, key) {
				acc[key] = m.x[key] || self.x[key]
				return acc
			}, {})
			return MergeR(value)
		},
		contractObj
	)
